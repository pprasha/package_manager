Set-StrictMode -Version latest

function Disable-AutoUpdateConda {
    conda config --set auto_update_conda False
}

function Complete-Install {
    Disable-AutoUpdateConda
}

Export-ModuleMember -Function Complete-Install

